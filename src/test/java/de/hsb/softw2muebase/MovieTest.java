package de.hsb.softw2muebase;

import de.hsb.softw2muebase.model.Movie;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class MovieTest {

    @Test
    void equalsMovieNull() {
        var movie = new Movie(null, null);
        var movieEqual = new Movie(null, null);
        assertEquals(movie, movieEqual);
    }

    @Test
    void equalsMovie() {
        var movie = new Movie("null", 0L);
        var movieEqual = new Movie("null", 0L);
        assertEquals(movie, movieEqual);
    }

    @Test
    void equalsMovieSameNull() {
        var movie = new Movie(null, null);
        assertEquals(movie, movie);
    }

    @Test
    void equalsMovieNotEqualsName() {
        var movie = new Movie(null, 0L);
        var movie2 = new Movie("null", 0L);
        assertNotEquals(movie, movie2);
    }

    @Test
    void equalsMovieNotEqualsID() {
        var movie = new Movie("null", null);
        var movie2 = new Movie("null", 0L);
        assertNotEquals(movie, movie2);
    }

    @Test
    void equalsMovieWithNull() {
        var movie = new Movie(null, null);
        assertNotEquals(movie, null);
    }

    @Test
    void getIDGetsID() {
        var movie = new Movie(null, 0L);
        assertEquals(0, movie.getID());
    }
}
