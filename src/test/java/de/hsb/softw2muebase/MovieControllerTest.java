package de.hsb.softw2muebase;

import de.hsb.softw2muebase.controller.MovieController;
import de.hsb.softw2muebase.model.Movie;
import de.hsb.softw2muebase.util.MovieRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.transaction.AfterTransaction;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MovieControllerTest {

    @Autowired
    private MovieRepository repo;

    private MovieController movieController;

    @BeforeEach
    void reset() {
        movieController = new MovieController(repo);
    }

/*@Test
    void returnContainsGutenTag() {
        String expected = "Guten Tag";

        String result = movieController.greeting("Irgendwas");

        Assertions.assertThat(result).contains(expected);
    }*/

    @Test
    void getAllMoviesEmpty() {
        var movies = new ArrayList<Movie>();

        var result = movieController.getAll();
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movies, result.getBody());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Top-Gun"})
    void addMovie(String name) {
        var result = movieController.add(new Movie(name, null));
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertNull(result.getBody());
    }

    @Test
    void addMovieBad() {
        var result = movieController.add(new Movie(null, null));
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        assertNull(result.getBody());
    }

    @Test
    void getAllMovieFull() {
        var movies = new ArrayList<Movie>();
        movies.add(new Movie("Who am I", 0L));

        this.addMovie("Who am I");

        var result = movieController.getAll();
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movies, result.getBody());
    }

    @Test
    void getAllMovieMulti() {
        var movies = new ArrayList<Movie>();
        movies.add(new Movie("King Kong", 0L));
        movies.add(new Movie("300", 1L));

        this.addMovie("King Kong");
        this.addMovie("300");

        var result = movieController.getAll();
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movies, result.getBody());
    }

    @Test
    void getIDMovieEmpty() {
        var result = movieController.get("1");
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
        assertNull(result.getBody());
    }

    @Test
    void getIDMovieFull() {
        var movie = new Movie("GhostBusters", 0L);

        this.addMovie("GhostBusters");

        var result = movieController.get("0");
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movie, result.getBody());
    }

    @Test
    void getNameMovieEmpty() {
        var movies = new ArrayList<Movie>();

        var result = movieController.get("Scream");
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movies, result.getBody());
    }

    @Test
    void getNameMovieFull() {
        var movies = new ArrayList<Movie>();
        movies.add(new Movie("Prey", 0L));

        this.addMovie("Prey");

        var result = movieController.get("Prey");
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movies, result.getBody());
    }

    @Test
    void getNameMovieMulti() {
        var movies = new ArrayList<Movie>();
        movies.add(new Movie("Prey", 0L));
        movies.add(new Movie("Prey", 1L));

        this.addMovie("Prey");
        this.addMovie("Prey");

        var result = movieController.get("Prey");
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movies, result.getBody());
    }

    @Test
    void getNameMovieNone() {
        var movies = new ArrayList<Movie>();
        movies.add(new Movie("Scream", 1L));

        this.addMovie("Prey");
        this.addMovie("Scream");

        var result = movieController.get("Scream");
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(movies, result.getBody());
    }
}