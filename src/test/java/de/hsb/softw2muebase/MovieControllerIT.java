package de.hsb.softw2muebase;

import de.hsb.softw2muebase.model.Movie;
import de.hsb.softw2muebase.util.MovieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@Transactional
@AutoConfigureMockMvc
class MovieControllerIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private MovieRepository repo;


    /*@Test
    void returnsNameWithBaseStatement() throws Exception {
        mockMvc
                .perform(get("/greeting/Seyling"))
                .andExpect(status().isOk())
                .andExpect(content().string("Guten Tag Seyling"));
    }*/

    @Test
    void getAllMoviesEmpty() throws Exception {
        mockMvc
                .perform(get("/movies/"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Top-Gun"})
    void addMovie(String name) throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.post("/movies/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"" + name + "\"}"))
                .andExpect(status().isCreated())
                .andExpect(content().string(""));
    }

    @Test
    void getAllMovieFull() throws Exception {
        addMovie("Who am I");
        mockMvc
                .perform(get("/movies/"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        [{"id":0,"name":"Who am I"}]"""));
    }

    @Test
    void getAllMovieMulti() throws Exception {
        addMovie("King Kong");
        addMovie("300");
        mockMvc
                .perform(get("/movies/"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        [{"id":0,"name":"King Kong"},{"id":1,"name":"300"}]"""));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Die Unglaublichen"})
    void addMovieBad(String name) throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.post("/movies/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"namos\": \"" + name + "\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(""));
    }

    @Test
    void getAllMovieBadGoodOnce() throws Exception {
        addMovieBad("Die Unglaublichen");
        addMovie("San Andreas");
        mockMvc
                .perform(get("/movies/"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        [{"id":0,"name":"San Andreas"}]"""));
    }

    @Test
    void getIDMovieEmpty() throws Exception {
        mockMvc
                .perform(get("/movies/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
        mockMvc
                .perform(get("/movies/0"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }

    @Test
    void getIDMovieFull() throws Exception {
        addMovie("GhostBusters");

        mockMvc
                .perform(get("/movies/0"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        {"id":0,"name":"GhostBusters"}"""));
    }

    @Test
    void getNameMovieEmpty() throws Exception {
        mockMvc
                .perform(get("/movies/Scream"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        []"""));
    }

    @Test
    void getNameMovieFull() throws Exception {
        addMovie("Prey");

        mockMvc
                .perform(get("/movies/Prey"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        [{"id":0,"name":"Prey"}]"""));
    }

    @Test
    void getNameMovieMulti() throws Exception {
        addMovie("Prey");
        addMovie("Prey");

        mockMvc
                .perform(get("/movies/Prey"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        [{"id":0,"name":"Prey"},{"id":1,"name":"Prey"}]"""));
    }

    @Test
    void getNameMovieNone() throws Exception {
        addMovie("Prey");
        addMovie("Scream");

        mockMvc
                .perform(get("/movies/Scream"))
                .andExpect(status().isOk())
                .andExpect(content().string("""
                        [{"id":1,"name":"Scream"}]"""));
    }

}