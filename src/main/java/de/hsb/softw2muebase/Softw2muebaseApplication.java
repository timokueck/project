package de.hsb.softw2muebase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Softw2muebaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(Softw2muebaseApplication.class, args);
	}

}
