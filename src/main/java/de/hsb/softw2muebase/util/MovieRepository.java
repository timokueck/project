package de.hsb.softw2muebase.util;

import de.hsb.softw2muebase.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;


public interface MovieRepository extends JpaRepository<Movie, Integer> {
    Collection<Movie> findByName(String lastName);
    Optional<Movie> findById(long parseLong);
}
