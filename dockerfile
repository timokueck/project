#### STAGE 1: Build ####

FROM maven:3-openjdk-18-slim as build-env

RUN apt-get update && \
        apt-get upgrade -y

COPY pom.xml .

RUN mvn dependency:resolve

COPY .m2 .m2
COPY src src

RUN mvn -f pom.xml clean package install

### STAGE 2: Setup ###

FROM openjdk:18-bullseye

RUN apt-get update && \
    apt-get upgrade -y
	
RUN ls

WORKDIR /target

COPY --from=build-env /target/softw2muebase-*.jar .

RUN ls

CMD ["java", "-jar", "softw2muebase-0.0.1-SNAPSHOT.jar"]
